<?php
  $eid = "";

  if (isset($_POST["delete"])) {
      if(isset($_POST["eid"])) $eid=$_POST["eid"];
      header("HTTP/1.1 307 Temprary Redirect");
      header("Location: managerProfileDeleted.php");
  }
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VTThrift Manager Portal</title>

</head>
<body>
    <h2>Employee Profile Management</h2>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
        <label>Select an Employee ID: &nbsp;&nbsp;
            <select name="eid" id="employeeDropDown">
                <?php
                //dynamically generate the option elements based on product IDs in the database
                require_once("db.php");
                $sql = "SELECT eid FROM employee ORDER BY eid";
                $result = $mydb->query($sql);
                while($row=mysqli_fetch_array($result)){
                    echo "<option value='".$row["eid"]."'>".$row["eid"]."</option>";
                }
                ?>
            </select>
        </label></br>

    <input type="submit" name="delete" value="Delete" />
    </form>
    
</body>
</html>