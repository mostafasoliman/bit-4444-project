<style>
    thead{
    background: red;
    font-weight: normal;
    }

    tbody{
    background: orange;
    font-weight: normal;
    }
</style>

<?php
    $id=0;
    
    if(isset($_GET["id"])){
        $id=$_GET["id"];
    }

    //specify a select sql statement based on $id value
    require_once("db.php");
    $sql="";

    if($id==0){
        $sql="SELECT * FROM orders";
        $result = $mydb->query($sql);
    }
    else{
        $sql="SELECT * FROM orders WHERE OID = ".$id;
        $result = $mydb->query($sql);
    }

    //generate an html table
    echo "
    <table>
        <thead>
            <tr>
                <th>Order ID</th>
                <th>Date of Purchase</th>
                <th>Total Pre-tax Costs</th>
                <th>Total Tax</th>
                <th>Total Shipping Costs</th>
                <th>Total Costs</th>
                <th>Total Quantity</th>
                <th>Payment Status</th>
                <th>Shipment Status</th>
                
            </tr>
        </thead>
        ";

        echo "<tbody>";
        while($row = mysqli_fetch_array($result)){
            echo "<tr>";

            echo "<td>";
            echo $row["OID"];
            echo "</td>";

            echo "<td>";
            echo $row["DateOfPurchase"];
            echo "</td>";

            echo "<td>";
            echo $row["TotalPreTaxCosts"];
            echo "</td>";

            echo "<td>";
            echo $row["TotalTax"];
            echo "</td>";

            echo "<td>";
            echo $row["TotalShippingCosts"];
            echo "</td>";

            echo "<td>";
            echo $row["TotalCosts"];
            echo "</td>";

            echo "<td>";
            echo $row["TotalQuantity"];
            echo "</td>";

            echo "<td>";
            echo $row["PaymentStatus"];
            echo "</td>";

            echo "<td>";
            echo $row["ShipmentStatus"];
            echo "</td>";

            echo "</tr>";
        }
        echo "</tbody>";
?>