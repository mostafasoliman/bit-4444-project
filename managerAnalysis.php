<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VTThrift Manager Portal</title>
</head>
<body>
    <form method="get" action="managerAnalysisOutput.php">
        <label>Choose an Order: &nbsp;&nbsp;
        <select name="OID" id="oidDropDown">
            <?php
            //dynamically generate the option elements based on supplier IDs in the database
            require_once("db.php");
            $sql = "SELECT OID FROM orders ORDER BY OID";
            $result = $mydb->query($sql);
            while($row=mysqli_fetch_array($result)){
                echo "<option value='".$row["OID"]."'>".$row["OID"]."</option>";
            }
            ?>
        </select>
        </label><br/>
        <input type="submit" name="submit" value="Submit">
    </form> 
</body>
</html>