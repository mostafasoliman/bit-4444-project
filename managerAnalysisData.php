<?php
    //get the order id
    $OID=0;
    if (isset($_GET["OID"])){
        $OID = $_GET["OID"];
    }

    require_once("db.php");

    $sql="SELECT OID, TotalCosts AS Placeholder FROM orders WHERE OID=$OID";

    //execute the sql statement
    //generate the data in the JSON format
    $result = $mydb->query($sql);

    $data = array();
    for($x=0; $x<mysqli_num_rows($result); $x++) {
        $data[] = mysqli_fetch_assoc($result);
    }

    echo json_encode($data);
?>