<?php
  $Email="";
  $Epassword="";
  $remember="no";
  //$ELevel=0;
  $error = false;
  $loginOK = null;
  $levelOK = null;

  if(isset($_POST["submit"])){
    if(isset($_POST["Email"])) $Email=$_POST["Email"];
    if(isset($_POST["Epassword"])) $Epassword=$_POST["Epassword"];
    if(isset($_POST["remember"])) $remember=$_POST["remember"];

    if(empty($Email) || empty($Epassword)) {
      $error=true;
    }

    //set cookies for remembering the user name
    if(!empty($Email) && $remember =="yes"){
      setcookie("Email", $Email, time()+60*60*24, "/");
    }

    if(!$error){
      //check username and password with the database record
      require_once("db.php");
      $sql = "SELECT Epassword FROM employee WHERE Email='$Email'";
      $result = $mydb->query($sql);

      $row=mysqli_fetch_array($result);
      if ($row){
        if(strcmp($Epassword, $row["Epassword"]) ==0 ){
          $loginOK=true;
        } else {
          $loginOK = false;
        }
      }

      //check employee level
      $sql = "SELECT ELevel FROM employee WHERE Email='$Email'";
      $result = $mydb->query($sql);

      $row=mysqli_fetch_array($result);
      //$ELevel = $row[0];
      if($row[0] == 10){
        $levelOK = true;
      }
      else{
        $levelOK = false;
      }

      if($loginOK && $levelOK) {
        //set session variables
        session_start();
        $_SESSION["Email"] = $Email;
        $_SESSION["Epassword"] = $Epassword;
        Header("Location:managerHome.php");
      }
    }
  }


 ?>
<!doctype html>
<html>
<head>
  <title>VTThrift Manager Portal</title>
  <style>
    .errlabel {color:red};
  </style>
</head>
<body>
  <h3>VTThrift Manager Login Portal</h3>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    <table>
      <tr>
        <td>Email</td>
      </tr>
      <tr>
        <td><input type="email" name="Email" value="<?php 
        if(!empty($Email)) 
          echo $Email;
        else if(isset($_COOKIE['Email'])){
          echo $_COOKIE['Email'];
        }
        ?>" /><?php if($error && empty($Email)) echo "<span class='errlabel'> Please enter a valid manager email.</span>";?></td>
      </tr>
      <tr>
        <td>Password</td>
      </tr>
      <tr>
        <td><input type="password" name="Epassword" value="<?php if(!empty($Epassword)) echo $Epassword; ?>" /><?php if($error && empty($Epassword)) echo "<span class='errlabel'> Please enter a password</span>";?></td>
      </tr>
    </table>
    <table>
      <tr>
        <td><input type="checkbox" name="remember" value="yes"/><label>Remember me</label></td>
      </tr>
      <tr>
        <td><?php if(!empty($Email) && !empty($Epassword) && $loginOK==false) echo "<span class='errlabel'>Email and password do not match.</span>"; ?></td>
      </tr>
      <tr>
        <td><?php if(!is_null($levelOK) && $levelOK==false && $loginOK==true) echo "<span class='errlabel'>You do not have Manager privileges. Please login through the regular employee portal. </span>"; ?></td>
      </tr>
      <tr>
        <td><input type="submit" name="submit" value="Login" /></td>
      </tr>
    </table>
  </form>

</body>
</html>
