<!doctype html>
<html>
<head>
  <title>VTThrift Manager Portal</title>
</head>
<body>
  <?php
    $FirstName = "";
    $LastName = "";
    $Email = "";
    $Epassword = "";
    $Phone = "";
    $DOB = "";
    $SSN = "";
    $address = "";
    $ELevel = 1;
    $eid = "";
    
    if(isset($_POST["FirstName"])) $FirstName=$_POST["FirstName"];
    if(isset($_POST["LastName"])) $LastName=$_POST["LastName"];
    if(isset($_POST["Email"])) $Email=$_POST["Email"];
    if(isset($_POST["Epassword"])) $Epassword=$_POST["Epassword"];
    if(isset($_POST["Phone"])) $Phone=$_POST["Phone"];
    if(isset($_POST["DOB"])) $DOB=$_POST["DOB"];
    if(isset($_POST["SSN"])) $SSN=$_POST["SSN"];
    if(isset($_POST["address"])) $address=$_POST["address"];
    if(isset($_POST["ELevel"])) $ELevel=$_POST["ELevel"];
    if(isset($_POST["eid"])) $eid=$_POST["eid"];
  ?>

  <!-- display form data -->
  <?php
    echo "
    <table>
        <thead>
            <tr>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Password</th>
                <th>Phone</th>
                <th>DOB</th>
                <th>SSN</th>
                <th>Address</th>
                <th>Employee Level</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>$eid</td>
                <td>$FirstName</td>
                <td>$LastName</td>
                <td>$Email</td>
                <td>$Epassword</td>
                <td>$Phone</td>
                <td>$DOB</td>
                <td>$SSN</td>
                <td>$address</td>
                <td>$ELevel</td>
            </tr>
        </tbody>
    </table>"
  ?>

  <?php
    //add a new employee record if emptype is new. otherwise, modify the existing employee record
    require_once("db.php");

    if($eid==0) {
      $sql = "INSERT INTO employee(FirstName, LastName, Email, Epassword, Phone, DOB, SSN, address, ELevel) VALUES ('$FirstName', '$LastName', '$Email', '$Epassword', '$Phone', '$DOB', '$SSN', '$address', '$ELevel')";

      $result=$mydb->query($sql);

      if ($result==1) {
        echo "<p>A new employee profile has been created.</p>";
      }
    } else {
      //modify the employee record...
        $sql = "UPDATE employee SET FirstName = '$FirstName', LastName = '$LastName', Email = '$Email', Epassword = '$Epassword', Phone = '$Phone', DOB = '$DOB', SSN = '$SSN', address = '$address', ELevel = '$ELevel' WHERE eid = '$eid'";
        $result=$mydb->query($sql);

        if ($result==1) {
          echo "<p>An employee profile has been updated.</p>";
        }
    }
   ?>
</body>
</html>
