<?php
  $FirstName = "";
  $LastName = "";
  $Email = "";
  $Epassword = "";
  $Phone = "";
  $DOB = "";
  $SSN = "";
  $address = "";
  $ELevel = 1;
  $err = false;

  if (isset($_POST["submit"])) {
      if(isset($_POST["FirstName"])) $FirstName=$_POST["FirstName"];
      if(isset($_POST["LastName"])) $LastName=$_POST["LastName"];
      if(isset($_POST["Email"])) $Email=$_POST["Email"];
      if(isset($_POST["Epassword"])) $Epassword=$_POST["Epassword"];
      if(isset($_POST["Phone"])) $Phone=$_POST["Phone"];
      if(isset($_POST["DOB"])) $DOB=$_POST["DOB"];
      if(isset($_POST["SSN"])) $SSN=$_POST["SSN"];
      if(isset($_POST["address"])) $address=$_POST["address"];
      if(isset($_POST["ELevel"])) $ELevel=$_POST["ELevel"];

      if(!empty($FirstName) && !empty ($LastName) && !empty ($Email) && !empty ($Epassword) &&
        !empty ($Phone) && !empty ($DOB) && !empty ($SSN) && !empty ($address)) {
        header("HTTP/1.1 307 Temprary Redirect");
        header("Location: managerProfileUpdated.php");
      } else {
        $err = true;
      }
  }
 ?>

<!doctype html>
<html>
<head>
  <title>VTThrift Manager Portal</title>
  <style>
    .errlabel {color:red;}
  </style>
</head>

<body>
  <h2>Employee Profile Management</h2>
  <br/>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
    <label>Employee ID (leave blank for a new employee):</label>
    <input type="text" name="eid" maxlength="3" size="3"/>
    <br />

    <label>First Name:</label>
    <input name="FirstName" type="text" value="<?php echo $FirstName; ?>"/>
    <?php
      if ($err && empty($FirstName)) {
        echo "<label class='errlabel'>Error: Please enter a first name.</label>";
      }
    ?>
    <br />

    <label>Last Name:</label>
    <input name="LastName" type="text" value="<?php echo $LastName; ?>"/>
    <?php
      if ($err && empty($LastName)) {
        echo "<label class='errlabel'>Error: Please enter a last name.</label>";
      }
    ?>
    <br />

    <label>Email:</label>
    <input name="Email" type="text" value="<?php echo $Email; ?>"/>
    <?php
      if ($err && empty($Email)) {
        echo "<label class='errlabel'>Error: Please enter a valid email address.</label>";
      }
    ?>
    <br />

    <label>Password:</label>
    <input name="Epassword" type="password" value="<?php echo $Epassword; ?>"/>
    <?php
      if ($err && empty($Epassword)) {
        echo "<label class='errlabel'>Error: Please enter a valid password.</label>";
      }
    ?>
    <br />
    
    <label>Phone:</label>
    <input name="Phone" type="tel" maxlength="12" value="<?php echo $Phone; ?>"/>
    <?php
      if ($err && empty($Phone)) {
        echo "<label class='errlabel'>Error: Please enter a valid phone number.</label>";
      }
    ?>
    <br />

    <label>Date of Birth:</label>
    <input name="DOB" type="date" value="<?php echo $DOB; ?>"/>
    <?php
      if ($err && empty($DOB)) {
        echo "<label class='errlabel'>Error: Please enter your date of birth.</label>";
      }
    ?>
    <br />

    <label>SSN:</label>
    <input name="SSN" type="text" maxlength="9" value="<?php echo $SSN; ?>"/>
    <?php
      if ($err && empty($SSN)) {
        echo "<label class='errlabel'>Error: Please enter a valid social security number.</label>";
      }
    ?>
    <br />

    <label>Address:</label>
    <input name="address" type="text" value="<?php echo $address; ?>"/>
    <?php
      if ($err && empty($address)) {
        echo "<label class='errlabel'>Error: Please enter a valid address.</label>";
      }
    ?>
    <br />

    <label>Employee Level:</label>
      <select name="ELevel">
        <option value ="1" <?php if($ELevel==1) echo "selected";?>>1 (Regular Employee)</option>
        <option value ="2" <?php if($ELevel==2) echo "selected";?>>2</option>
        <option value ="3" <?php if($ELevel==3) echo "selected";?>>3</option>
        <option value ="4" <?php if($ELevel==4) echo "selected";?>>4</option>
        <option value ="5" <?php if($ELevel==5) echo "selected";?>>5</option>
        <option value ="6" <?php if($ELevel==6) echo "selected";?>>6</option>
        <option value ="7" <?php if($ELevel==7) echo "selected";?>>7</option>
        <option value ="8" <?php if($ELevel==8) echo "selected";?>>8</option>
        <option value ="9" <?php if($ELevel==9) echo "selected";?>>9</option>
        <option value ="10" <?php if($ELevel==10) echo "selected";?>>10 (Manager)</option>
    </select>
    <br />
    <br />

    <input type="submit" name="submit" value="Submit" />
    <br />
  </form>

</body>
</html>
