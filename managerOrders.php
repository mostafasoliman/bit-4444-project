<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VTThrift Manager Portal</title>
    <script src="jquery-3.1.1.min.js"></script>
    <script>
        //displays table
        var asyncRequest;
        var id;

        function getAllOrders(){
            //the event handler for the mouseover event of the all orders link (Javascript ajax)
            
            try {
				asyncRequest = new XMLHttpRequest();  //create request object

				//register event handler
                asyncRequest.onreadystatechange=stateChange;
                var url = "managerOrdersView.php";
				asyncRequest.open('GET',url,true);  // prepare the request
				asyncRequest.send(null);  // send the request
				}
			catch (exception) {alert("Request failed");}
                
        function stateChange() 
        {
			// if request completed successfully
			if(asyncRequest.readyState==4 && asyncRequest.status==200) {
				document.getElementById("contentArea").innerHTML=
					asyncRequest.responseText;  // places text in contentArea
			}
       }
    }

       function clearPage(){
            document.getElementById("contentArea").innerHTML="";
       }

       function init(){
           //set up the event handlers for the mouseover, mouseout events on the all orders link
           var c = document.getElementById("orderLink");
           c.addEventListener("mouseover", getAllOrders);
           c.addEventListener("mouseout", clearPage);
       }

       document.addEventListener("DOMContentLoaded", init);

       //jQuery ajax
       //add the selected order id at the end of the url
       //e.g., managerOrdersView.php?pid=1
       $(function()
       {
        $("#orderDropDown").change(function()
        {
          //get content from the server using XMLHttpRequest
            id = document.getElementById("orderDropDown").value;
            $.ajax({
              url:"managerOrdersView.php?id="+id,
              async:true,
              success: function(result)
              {
                $("#contentArea").html(result);
              }
            })
          })
        })

    </script>
</head>
<body>
    <h2>Open Orders</h2>
    <a id="orderLink" href="">View All Orders</a><br/>
       <label>Choose an Order: &nbsp;&nbsp;
        <select name="oid" id="orderDropDown">
            <?php
            //dynamically generate the option elements based on order IDs in the database
            require_once("db.php");
            $sql = "SELECT OID FROM orders ORDER BY OID";
            $result = $mydb->query($sql);
            while($row=mysqli_fetch_array($result)){
                echo "<option value='".$row["OID"]."'>".$row["OID"]."</option>";
            }
            ?>
        </select>
        </label><br/>
    <div id="contentArea">&nbsp;</div>
</body>
</html>