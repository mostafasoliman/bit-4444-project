<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VTThrift Manager Portal</title>
</head>
<body>
    <h1>VTThrift Management System</h1>
    <ul>
        <li>Employee Profile Management</li>
            <ul>
                <li><a href="managerProfileUpdate.php">Create/Modify Employee Profiles</a></li>
                <li><a href="managerProfileDelete.php">Delete Employee Profiles</a></li>
            </ul>
        <li><a href="managerOrders.php">View Orders</a></li>
        <li><a href="managerAnalysis.php">Monthly Order Analysis</a></li>
        <li><a href="managerLogout.php">Log Out</a></li>
    </ul>
</body>
</html>