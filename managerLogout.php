<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VTThrift Manager Portal</title>
</head>
<body>
    <?php
        session_start();
        $Email = $_SESSION["Email"];
        $Epassword = $_SESSION["Epassword"];
        $_SESSION = array();
        session_destroy();

        echo "<p>Goodbye, ".$Email."</p>";
        echo "You have successfully logged out. Please <a href='managerLogin.php'>click here to login again</a>";
    ?>
</body>
</html>